use std::path::Path;
use std::{env, fs};

fn main() {
    let cachedir = format!(
        "{}/.cache/calcbatt",
        env::var("HOME").expect("No HOME specified")
    );

    let stat = "/sys/class/power_supply/BAT0/status";
    let charging = "/sys/class/power_supply/AC/online";
    let cap = "/sys/class/power_supply/BAT0/capacity";
    let watt = "/sys/class/power_supply/BAT0/power_now";
    let watthr = "/sys/class/power_supply/BAT0/energy_now";
    let watthrmax = "/sys/class/power_supply/BAT0/energy_full";
    let watthrmaxd = "/sys/class/power_supply/BAT0/energy_full_design";

    let mut stat = slurp(stat);
    let charging = read_u64(charging) > 0; //cast to bool
    let mut cap = read_u64(cap);
    let mut watt = read_u64(watt);
    let mut watthr = read_u64(watthr);
    let mut watthrmax = read_u64(watthrmax);
    let watthrmaxd = read_u64(watthrmaxd);

    let cache_statf = &format!("{}/status_last", cachedir);
    let cache_capf = &format!("{}/capacity_last", cachedir);
    let cache_wattf = &format!("{}/power_avg", cachedir);
    let cache_watthrf = &format!("{}/energy_last", cachedir);

    if watthrmax > watthrmaxd {
        watthrmax = watthrmaxd;
        cap = (watthr * 100) / watthrmaxd;
    }

    let mut valid = cap <= 100 && watt > 0;

    // Battery status is "unknown" but AC is plugged in
    if stat == "Unknown" && charging {
        stat = "Charging".to_string();
    }

    // Battery claims to be charging but reports full energy:
    if stat == "Charging" && watthr == watthrmax {
        valid = false;
    }

    fs::create_dir_all(&cachedir).expect(&format!("Couldn't open or make directory {}", cachedir));

    if valid {
        // store valid values
        fs::write(cache_capf, &format!("{}\n", cap)).expect("can't write cap");
        fs::write(cache_watthrf, &format!("{}\n", watthr)).expect("can't write watthr");

        if stat == "Discharging" {
            // avg of wattage
            let cache_watt = read_u64(cache_wattf);
            if cache_watt > 0 {
                watt = (cache_watt + watt) / 2;
            }
            fs::write(cache_wattf, &format!("{}\n", watt)).expect("can't write watt");
        }
    } else {
        cap = read_u64(cache_capf);
        watt = read_u64(cache_wattf);
        watthr = read_u64(cache_watthrf);
    }

    if stat == "Unknown" {
        stat = slurp(cache_statf);
    } else {
        fs::write(cache_statf, &format!("{}\n", stat)).expect("can't write stat");
    }

    if stat == "Charging" {
        println!("{}% (Chrg)", cap);
        if Path::new(cache_wattf).exists() {
            fs::remove_file(cache_wattf).expect("couldn't delete calcbatt cache watt");
        }
        return;
    } else if stat == "Full" {
        println!("Full");
        if Path::new(cache_wattf).exists() {
            fs::remove_file(cache_wattf).expect("couldn't delete calcbatt cache watt");
        }
        return;
    }

    // Format battery life time remaining
    let hrs: f64  = watthr as f64 / watt as f64;
    let hrsi      = hrs as u64;
    let mins: f64 = (hrs - hrsi as f64) * 60f64;
    let minsi     = mins as u64;
    let secs: f64 = (mins - minsi as f64) * 60f64;
    let secsi     = secs as u64;

    println!("{}% {}:{:0>2}:{:0>2}", cap, hrsi, minsi, secsi);
}

fn read_u64(filename: &str) -> u64 {
    slurp(filename).parse().unwrap_or(0)
}

fn slurp(filename: &str) -> String {
    fs::read_to_string(filename)
        .unwrap_or("".into())
        .trim()
        .into()
}
