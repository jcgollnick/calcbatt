# Battery Calculator

This reads the µWh, µW and capacity (%) of the battery on `/sys/class/power_supply/BAT0`.
I wrote this because my ThinkPad X1 Extreme gen 2 has some wonky ass shit where
the battery values will report wacky numbers from time to time, and using `acpi`
also had a problem where my Logitech wireless mouse (USB receiver) would show up
on Battery 0 instead of my actual laptop battery.

To remedy my woes, I wrote this little program to poll the battery specs and cache good values,
and refer to the cache when the values are bad (when `capacity` is over 100% or when
`power_now` (µW) is 0). Thus I can stick this in my i3 status bar and never look at
the wrong battery or wacky time estimates again.

It also runs faster than `acpi` -- check this shit out:
```bash
[grendel@artix ~/docs/rust/calcbatt]
 $ time for i in {1..20000}; do calcbatt >/dev/null; done

real	0m15.325s
user	0m11.441s
sys	0m4.365s
[grendel@artix ~/docs/rust/calcbatt]
 $ time for i in {1..20000}; do acpi >/dev/null; done

real	0m24.394s
user	0m13.498s
sys	0m5.526s
```
#neato

### Installation

just clone the repo and build it it's not that hard

```bash
$ git clone http://gitlab.com/cptchuckles/calcbatt.git
$ cd calcbatt
$ cargo build --release
```
but you need [rust](https://www.rust-lang.org/) tho
